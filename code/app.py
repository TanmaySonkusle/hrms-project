from flask import Flask
from modules.dashboard.dashboard import dashboard, UPLOAD_FOLDER, mail
from modules.auth.auth import auth
from modules.leave.leave import leave
from modules.reimbursement.reimbursement import reimbursement
from modules.onboarding.onboarding import onboarding
from db import db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///HRMS'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = "hrms"
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql:///HRMS"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.config['DEBUG'] =True
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'hrms.work.11@gmail.com'
app.config['MAIL_DEFAULT_SENDER'] = 'hrms.work.11@gmail.com'
app.config['MAIL_PASSWORD'] = '123@qwer'
app.config['MAIL_ASCII_ATTACHMENTS'] = True
app.register_blueprint(dashboard)
app.register_blueprint(auth)
app.register_blueprint(leave)
app.register_blueprint(reimbursement)
app.register_blueprint(onboarding)

@app.before_first_request
def create_table():
    db.create_all()

if __name__ == "__main__":
    db.init_app(app)
    mail.init_app(app)
    app.run(debug=True)
