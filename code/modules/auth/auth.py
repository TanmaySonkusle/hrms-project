from flask import Blueprint,g,session,request, redirect, flash,url_for,render_template
from werkzeug.security import generate_password_hash,check_password_hash
from db import db
from modules.dashboard.dashboard import Employee
auth = Blueprint('auth',__name__)

@auth.route('/',methods=['GET','POST'])
@auth.route('/login',methods=['GET','POST'])
def login():
    if(request.method == "POST"):
        email = request.form['email']
        password = request.form['password']
        emp_details = Employee.query.filter_by(email = email).first()
        if emp_details is None:
            flash("No user found")
            return redirect(url_for('auth.login'))
        elif emp_details.password == password:
            session.clear()
            session['email'] = emp_details.email
            return redirect(url_for('profile.home'))
        else:
            flash("Password Incorrect")
            return redirect(url_for('auth.login'))
    return render_template('login.html')

@auth.route('/logout',methods=['GET','POST'])
def logout():
    session.clear()
    return redirect(url_for('auth.login'))

@auth.before_app_request
def load_user():
    user_email = session.get('email')
    
    if user_email is None:
        g.user = None
    else:
        g.user = Employee.query.filter_by(email = user_email).first()


