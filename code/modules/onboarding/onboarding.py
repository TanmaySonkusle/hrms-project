from flask import Flask, request, render_template,Blueprint
from db import db
import csv
from modules.dashboard.dashboard import Employee


onboarding = Blueprint('onboarding', __name__, url_prefix='/')

@onboarding.route('/onboarding',methods=["GET","POST"])
def onboard():
    return render_template('onboarding.html')

@onboarding.route('/onboarding/success', methods = ["POST"])
def onboard_processing():
    if request.method == "POST":    
        f = request.files['file']
        f.save(f.filename)
        csv_file = open(f.filename, 'r')
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            employee = Employee(
                name = row['name'],
                email = row['email'],
                designation = row['designation'],
                age = row['age'],
                mobile_no = row['mobile_no'],
                gender = row['gender'],
                super_id = row['super_id']
                )
            db.session.add(employee)

        db.session.commit()
        return "done"