from flask import Blueprint, render_template, request, Flask, flash, redirect,g,url_for
from db import db


class LeaveRequest(db.Model):
    leave_id = db.Column(db.Integer, primary_key=True,unique=True, nullable=False)
    id = db.Column(db.Integer, nullable=False)
    leave_type = db.Column(db.Text, nullable=False)
    start_date = db.Column(db.DateTime,  nullable=False)
    end_date = db.Column(db.DateTime, nullable=False)
    message = db.Column(db.Text, nullable=False)
    status = db.Column(db.Text, nullable=False)
    super_id = db.Column(db.Integer, nullable=False)


leave = Blueprint('leave', __name__, url_prefix='/')


@leave.route('/leave', methods=["GET", "POST"])
def leaverequest():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    return render_template('leave.html')


@leave.route('/leaveprocess', methods=['GET',"POST"])
def save_data():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    if request.method == "POST":
        temp = LeaveRequest(
            id=g.user.id,
            leave_type=request.form['leave_type'],
            start_date=request.form['start_date'],
            end_date=request.form['end_date'],
            message=request.form['comments'],
            status='Pending',
            super_id=g.user.super_id,
        )

    db.session.add(temp)
    db.session.commit()
    flash("Your Request is sent for approval","success")
    return redirect("/leave")