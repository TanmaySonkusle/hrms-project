from flask import Blueprint, render_template, request, redirect, url_for,g,flash
from db import db
from flask_mail import Mail, Message
import csv
import uuid     #to generate random string

class Employee(db.Model):
    id = db.Column(db.Integer, primary_key = True, unique = True, nullable = False)
    name = db.Column(db.Text, nullable = False)
    email = db.Column(db.Text, unique = True, nullable = False)
    password = db.Column(db.Text, nullable = False)
    mail = db.Column(db.Text, default = "NOT_SENT")
    designation = db.Column(db.Text, nullable = False)
    age = db.Column(db.Integer, nullable = False)
    mobile_no = db.Column(db.Text, unique = True, nullable = False)
    gender = db.Column(db.Text, nullable = False)
    super_id = db.Column(db.Integer, nullable = False)

dashboard = Blueprint('profile', __name__, url_prefix='/')
UPLOAD_FOLDER = '/code/modules/dashboard'
mail = Mail()

@dashboard.route('/home',methods=['GET','POST'])
def home():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    return render_template('home.html')

@dashboard.route('/reimbursement',methods=['GET','POST'])
def reimbursement():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    return render_template('reimbursement.html')

@dashboard.route('/onboarding',methods=["GET","POST"])
def onboarding():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    if g.user.designation != 'developer':
        return redirect(url_for('profile.onboard_processing'))    
    else:
        return render_template('noaccess.html')
@dashboard.route('/profile',methods=["GET","POST"])
def profile():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    return render_template('profile.html')

@dashboard.route('/leave',methods=["GET","POST"])
def leave():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    return render_template('leave.html')
@dashboard.route('/onboarding_success', methods = ["GET","POST"])
def onboard_processing():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    if request.method == "POST":    
        f = request.files['file']
        f.save(f.filename)
        csv_file = open(f.filename, 'r')
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            password = uuid.uuid4().hex[0:8]
            employee = Employee(
                name = row['name'],
                email = row['email'],
                designation = row['designation'],
                age = row['age'],
                mobile_no = row['mobile_no'],
                gender = row['gender'],
                super_id = row['super_id'],
                password = password
                )
            db.session.add(employee)
        try:
            db.session.commit()
            employee_data = Employee.query.filter_by(mail='NOT_SENT').all()
            if employee_data is None:
                 flash("Emails are all sent",'warning')
                 return render_template('onboarding.html')
            with mail.connect() as conn:
                for employee in employee_data:
                    message = f'''
                                Hi {employee.name}, This is a test body
                                Here are your login credentials for HRMS!
                                email:-  {employee.email}
                                password:-  {employee.password}
                        '''
                    msg = Message(recipients=[employee.email],body=message ,subject="LOGIN CREDENTIALS")
                    conn.send(msg)
                    employee.mail = "SENT"
            db.session.commit()    
            flash("Successfully added the entries",'success')
            return render_template('onboarding.html')
        except:
            flash("Duplicate entries found in the database",'danger')
            return render_template('onboarding.html')
        
    return render_template('onboarding.html')

