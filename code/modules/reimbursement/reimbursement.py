from flask import Flask, render_template, request, Blueprint, redirect, flash,g,url_for
from db import db

class Reimbursement(db.Model):
    r_id = db.Column(db.Integer,primary_key = True,unique = True, nullable = True)
    id = db.Column(db.Integer, nullable = False)
    super_id = db.Column(db.Integer, nullable = False)
    title = db.Column(db.Text, nullable = False)
    reim_type = db.Column(db.Text, nullable = False)
    date = db.Column(db.DateTime, nullable = False)
    amount = db.Column(db.Float, nullable = False)
    comment = db.Column(db.Text, nullable = True)

reimbursement = Blueprint('reimbursement', __name__, url_prefix='/')


@reimbursement.route('/reimbursement',methods=['GET','POST'])
def reimbursement_request():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    return render_template('reimbursement.html')

@reimbursement.route('/reimbursement/success', methods=['GET','POST'])
def reim_processing():
    if g.user is None:
        flash("You're not logged in. Log in first")
        return redirect(url_for('auth.login'))
    if request.method == "POST":
        reimburse = Reimbursement(
            id = g.user.id,
            super_id = g.user.super_id,
            title = request.form['title'],
            reim_type = request.form['type'],
            date = request.form['date'],
            amount = request.form['amount'],
            comment = request.form['comment']
        )

        db.session.add(reimburse)
        db.session.commit()
        flash("Your request is successfully sent",'success')
        return redirect('/reimbursement')